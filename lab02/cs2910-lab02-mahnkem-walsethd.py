# coding=utf-8
# cs2910-lab02-mahnkem-walsethd.py Lab 2 -- UDP/TCP send/receive
# Team members: Matthew Mahnke (mahnkem), Dylan Walseth (walsethd)

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# Port number definitions
# (May have to be adjusted if they collide with ports in use by other programs/services.)
UDP_PORT = 12010
TCP_PORT = 12100

# Host address when acting as "receiver" ("server").
# The address '' means accept any connection for our "receive" port from any network interface
# on this system (including 'localhost' loopback connection).
LISTEN_FOR_HOST = ''

# Address of the "other" ("server") host that should be connected to for "send" operations.
# When connecting on one system, use 'localhost'
OTHER_HOST = 'localhost'
# When "sending" to another system, use its IP address (or DNS name if there it has one)
# OTHER_HOST = '155.92.x.x'

def main():
    # Get chosen operation from the user.
    action = raw_input('Select "(1-US) udpsend", "(2-UR) udpreceive", "(3-TS) tcpsend", or "(4-TR) tcpreceive":')
    # Execute the chosen operation.
    if action in ['1', 'US', 'us', 'udpsend']:
        udp_send(OTHER_HOST, UDP_PORT, 'UDP test message')
    elif action in ['2', 'UR', 'ur', "udpreceive"]:
        udp_receive(UDP_PORT);
    elif action in ['3', 'TS', 'ts', 'tcpsend']:
        tcp_send(OTHER_HOST, TCP_PORT, 'TCP test message');
    elif action in ['4', 'TR', 'tr', 'tcpreceive']:
        tcp_receive(TCP_PORT);
    else:
        print "Unknown action: '{0}'".format(action)


# Send a UDP message to a designated host/port
# Close socket
# Return
def udp_send(dst_host, dst_port, message):
    print "udp_send: dst_host='{0}', dst_port={1}, message='{2}'".format(dst_host, dst_port, message)
    foo = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    foo.connect((dst_host, dst_port))
    foo.send(message)
    foo.close()


# Listen for a UDP message on a designated port
# Print the address and port of the sender
# Print the message length and message string
# Close the socket
# Return
def udp_receive(listen_port):
    print "udp_receive: listen_port={0}".format(listen_port)
    this_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    this_socket.bind((LISTEN_FOR_HOST, UDP_PORT))
    message = this_socket.recv(1024)
    print "length={0}, message='{1}'".format(len(message), message)
    this_socket.close()


# Send a TCP message to a designated host/port.
# Receive a one-character response from the "server".
# Print the received response.
# Close the socket
# Return
def tcp_send(server_host, server_port, message):
    print "tcp_send: dst_host='{0}', dst_port={1}, message='{2}'".format(server_host, server_port, message)
    bar = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    bar.connect((server_host, server_port))
    bar.send(message)
    response = bar.recv(1024)
    print 'Server response:', response
    bar.close()


# Listen for a TCP connection on a designated "listening" port
# Accept the connection, creating a connection socket
# Print the address and port of the sender
# Receive the message string (one "socket.recv" call is sufficient for now)
# Print the message length and message string
# Send a single-character response (e.g., "Y") back to the sender
# Close the connection socket
# Close the listening socket
# Return
def tcp_receive(listen_port):
    print "tcp_receive (server): listen_port={0}".format(listen_port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((LISTEN_FOR_HOST, listen_port))
    sock.listen(1)
    (data_sock, address) = sock.accept()
    message = data_sock.recv(1024)
    print "length={0}, message='{1}'".format(len(message), message)
    data_sock.send('K')
    sock.close()
    data_sock.close()

# Invoke the main method to run the program.
main()

# We setup (a) socket(s) to send and recieve using TCP and UDP, and printed any
# recieved messages. One issue we encountered was the UDP port we tried to use
# was in use by another process, so we changed it from 12000 to 12010.
# We learned more Python usage (in particular the socket class), and learned
# about the UDP and TCP protocols by communicating basic information between
# computers across a network.
