# coding=utf-8

# : ## Lab 5 ##
# :
# : CS-2910 Network Protocols
# : Dr. Sebern
# : Fall quarter 2014-2015
# :
# : | Team members            |
# : |:------------------------|
# : | Matthew Mahnke          |
# : | Dylan Walseth           |
# :

# Socket library
import socket

# SSL/TLS library
import ssl

# base-64 encode/decode
import base64

# Python date/time and timezone modules
import datetime
import time

# Module for reading password from console without echoing it
import getpass

# Modules for some file operations
import os
import mimetypes

# pytz timezone name for our local timezone
import pytz

import re
import sys

TIMEZONE_NAME = 'US/Central'

# Host name for MSOE (hosted) SMTP server
SMTP_SERVER = 'smtp.office365.com'

# Sender/user email
# Used as "From" and as username for authentication
# Modify as needed
SMTP_USER = 'walsethd@msoe.edu'

# SMTP domain name
SMTP_DOMAINNAME = 'msoe.edu'


# Main test method to send an SMTP email message
# Modify data as needed/desired to test your code,
#	but keep the same interface for the smtp_send
#	method.
def main():
    message_info = {}
    message_info['To'] = 'walsethd@msoe.edu'
    message_info['From'] = SMTP_USER
    message_info['Subject'] = 'Yet another test message'
    message_info['Date'] = 'Thu, 9 Oct 2014 23:56:09 +0000'
    message_info['Date'] = get_formatted_date()

    print "message_info =", message_info

    message_text = 'Test message_info number 6\r\n\r\nAnother line.'

    # Prompt user for email password
    password = getpass.getpass()

    smtp_send(password, message_info, message_text)


# Send a message via SMTP.
#	password -- string containing user password
#	message_info -- dictionary with string values for the following keys:
#			'To': Recipient address (only one recipient required)
#			'From': Sender address
#			'Date': Date string for current date/time in SMTP format
#			'Subject': Email subject
#		Other keys can be added to support other email headers, etc.
#	Returns:
#		No return value
def smtp_send(password, message_info, message_text):
    sck = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sck.connect((SMTP_SERVER, 587))  # mail submission=587; SMTPS=465
    # print get_server_response(sck)

    sck.send('EHLO ' + SMTP_USER.split('@')[1] + '\r\n')

    # Receive some lines starting with "250-"; last line starts "250 "
    response = get_server_response(sck)
    check_status_code(response, 250, sck)
    # Check for "STARTTLS"
    if re.search('STARTTLS', response) is not None:
        sck.send('STARTTLS\r\n')
        check_status_code(get_server_response(sck), 220, sck)
        # Wrap socket
        ssl_skt = ssl.wrap_socket(sck, ssl_version=ssl.PROTOCOL_SSLv3)
        # Resend "EHLO"
        ssl_skt.send('EHLO ' + SMTP_USER.split('@')[1] + '\r\n')
        # receive response again with "AUTH LOGIN" instead of "STARTTLS"
        check_status_code(get_server_response(ssl_skt), 250, ssl_skt)
        # Send "AUTH LOGIN"
        ssl_skt.send('AUTH LOGIN\r\n')
        # Receive "334 dkfhsjdhfjklsh;fl" <-- "Username:"
        check_status_code(get_server_response(ssl_skt), 334, ssl_skt)
        # Send base64.encode(SMTP_USER)
        ssl_skt.send(base64.b64encode(SMTP_USER) + '\r\n')
        # Receive "334 fjsdflas;lddfjls" <-- "Password:"
        check_status_code(get_server_response(ssl_skt), 334, ssl_skt)
        # Send base64.encode(password)
        ssl_skt.send(base64.b64encode(password) + '\r\n')
        # Receive "235 ..." good auth
        check_status_code(get_server_response(ssl_skt), 235, ssl_skt)

        # point the (previously non-SSL/TLS) socket to the SSL/TLS socket to improve code re-use
        sck = ssl_skt

    # Send "MAIL FROM <SMTP_USER>"
    sck.send('MAIL FROM:<{0}>\r\n'.format(SMTP_USER))
    # Receive "250 OK"
    check_status_code(get_server_response(sck), 250, sck)
    # Send "RCPT TO:<message_info['To']>"
    sck.send('RCPT TO:<{0}>\r\n'.format(message_info['To']))
    # Receive "250 OK"
    check_status_code(get_server_response(sck), 250, sck)
    # Send "DATA"
    sck.send('DATA\r\n')
    # Receive "354 start mail input"
    check_status_code(get_server_response(sck), 354, sck)
    # Send message_info
    sck.send(convert_dict(message_info))
    # Send "CRLF"
    sck.send('\r\n')
    # Send message_text
    sck.send(message_text + '\r\n')
    # Send "CRLF.CRLF"
    sck.send('\r\n.\r\n')
    # Receive "250"
    check_status_code(get_server_response(sck), 250, sck)
    # Send "QUIT"
    sck.send('QUIT\r\n')
    # Receive "221 SMTP_USER.split('@')[1] Service closing transmission channel"
    check_status_code(get_server_response(sck), 221, sck)

    sck.close()


# Checks the servers response for a match to the specified desired status code.
# If the status codes do not match, a 'QUIT' message is sent to the sock, the sock is closed, and the program exits.
#   server_response -- (string) the server's response
#   desired_code -- (int) our desired status code
#   sock -- (socket) the socket to send a 'QUIT' command to
#   Returns:
#         the server_response if the status codes match
def check_status_code(server_response, desired_code, sock):
    # the status will always be the first 3 characters
    actual_code = int(server_response[:3])
    if (actual_code / 100) != (desired_code / 100):
        print 'Exiting program. Received status code {0}, expected {1}'.format(actual_code, desired_code)
        sock.send('QUIT')
        sock.close()
        sys.exit(-1)


# Returns one line from the socket (delimited of CRLF)
def get_line(request_sock):
    line = ''
    while not line.endswith('\r\n'):
        line += request_sock.recv(1)
    return line.strip()


# Returns a server's response
# request_socket -- the socket from which to get the server's response (uses get_line())
# Returns:
#   A string representation of the server's response on a single line
def get_server_response(request_socket):
    lines = [get_line(request_socket)]
    while not re.match('\d{3} .*', lines[-1]):
        lines.append(get_line(request_socket))

    status_code = lines[0][:3]

    response = ''
    for line in lines:
        if re.match('\d{3}[- ].*', line):
            line = line.lstrip('0123456789- ')
        response += line + ' '

    return status_code + ' ' + response.strip()


# Converts a dictionary into the properly formatted SMTP equivalent.
# dictionary -- the dictionary to be converted
#   Returns:
#       a properly formatted SMTP-header equivalent for inserting into HTTP response headers
def convert_dict(dictionary):
    smtp_dict = ''
    for key, value in dictionary.iteritems():
        smtp_dict += str(key) + ':' + str(value) + '\r\n'
    return smtp_dict


# ** Do not modify code below this line.

# Utility functions
# You may use these functions to simplify your code.

# Get the current date and time, in a format suitable for an email date header.
# The constant TIMEZONE_NAME should be one of the standard pytz timezone names.
# If you really want to see them all, call the print_all_timezones function.
# Returns:
#		Formatted current date/time value, as a string
def get_formatted_date():
    zone = pytz.timezone(TIMEZONE_NAME)
    print "zone =", zone
    timestamp = datetime.datetime.now(zone)
    timestring = timestamp.strftime('%a, %d %b %Y %H:%M:%S %z')  # Sun, 06 Nov 1994 08:49:37 +0000
    return timestring


# Print all pytz timezone strings
def print_all_timezones():
    for tz in pytz.all_timezones:
        print tz


# You probably won't need the following methods, unless you decide to
# try to handle email attachments or send multi-part messages.
# These advanced capabilities are not required for the lab assignment.

# Try to guess the MIME type of a file (resource), given its path (primarily its file extension)
#	file_path -- string containing path to (resource) file, such as './abc.jpg'
#	Returns:
#		If successful in guessing the MIME type, a string representing the content type, such as 'image/jpeg'
#		Otherwise, None
def get_mime_type(file_path):
    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


# Try to get the size of a file (resource) in bytes, given its path
#	file_path -- string containing path to (resource) file, such as './abc.html'
#	Returns:
#		If file_path designates a normal file, an integer value representing the the file size in bytes
#		Otherwise (no such file, or path is not a file), None
def get_file_size(file_path):
    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()

# A description of the functionality you implemented and the results of your testing.
# We implemented a simple smtp client which contacts a server and requests attempts to send an email using the
# given information. The client requests the password for the account and attempts to go through the steps required to
# send an email.
# We were unable to get the client to properly contact the server so we implemented everything the best we could as we
# were unable to test it. (See email thread about recv issue.)
