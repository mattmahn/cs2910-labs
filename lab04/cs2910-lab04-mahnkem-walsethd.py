#!/usr/bin/env python2
# coding=utf-8

# : ## Lab 4 ##
# :
# : CS-2910 Network Protocols
# : Dr. Sebern
#: Fall quarter 2014-2015
#:
#: | Team members            |
#: |:------------------------|
#: | Matthew Mahnke          |
#: | Dylan Walseth           |
#:

import socket
import string
import threading
import os
import mimetypes
import datetime


def main():
    http_server_setup(8082)


# Start the HTTP server
#	Open the listening socket
#	Accept connections and spawn processes to handle requests
#	port -- listening port number
def http_server_setup(port):
    NUM_CONNECTIONS = 10
    run_flag = True
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_address = ('', port)
    server_socket.bind(listen_address)
    server_socket.listen(NUM_CONNECTIONS)
    print 'Listening on localhost:{}\n'.format(port)
    try:
        while run_flag:
            request_socket, request_address = server_socket.accept()
            print 'connection from {0} {1}'.format(request_address[0], request_address[1])
            # Create a new thread, and set up the handle_request method and its argument (in a tuple)
            request_handler = threading.Thread(target=handle_request, args=(request_socket,))
            # Start the request handler thread.
            request_handler.start()
            # Just for information, display the running threads (including this main one)
            print 'threads: ', threading.enumerate()
    # Set up so a Ctrl-C should terminate the server; this may have some problems on Windows
    except KeyboardInterrupt:
        print "HTTP server exiting . . ."
        print 'threads: ', threading.enumerate()
        server_socket.close()
        run_flag = False


# Handle a single HTTP request, running on a newly started thread.
#	request_socket -- socket representing TCP connection from the HTTP client_socket
#	Returns: nothing
#		Closes request socket after sending response.
#		Should include a response header indicating NO persistent connection
def handle_request(request_socket):
    request_line, request_header_fields = get_header(request_socket)
    print 'request_line =', request_line
    print 'request_header_fields =', request_header_fields

    request_line_split = request_line.split(' ')
    resource = request_line_split[1]
    if resource == '/':
        request_line_split[1] = resource = './index.html'
    elif resource.startswith('/'):
        request_line_split[1] = resource = '.' + resource
    response = make_status_code(request_line_split) + '\r\n'
    response_dict = dict()
    response_dict['Date'] = datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
    response_dict['Connection'] = 'close'

    resource_data = open(resource, 'rb').read()
    response_dict['Content-Type'] = get_mime_type(resource)
    response_dict['Content-Length'] = len(resource_data)

    response += convert_dict(response_dict)
    response += '\r\n'
    response += resource_data
    request_socket.send(response)
    request_socket.close()


# Converts a list to a dictionary.
#   list_to_convert -- the list to convert to a dictionary
#
#   Returns a dictionary representing the specified list
def make_dict(list_to_convert):
    dictionary = dict()
    for item in list_to_convert:
        key, value = make_line_tuple(item)
        dictionary[key] = value
    return dictionary


# Returns a tuple representation of the specified line.
#   Returns:
#       The first value is what is in front of a ':' or '=' and the second is what is after.
def make_line_tuple(line):
    line_lst = string.split(line, ':', 1)
    return line_lst[0].strip(), line_lst[1].strip()


# Get the header from the socket
#   Returns:
#       A tuple whose first argument is the request line, and the second is a dictionary of
#       header fields.
def get_header(request_socket):
    request_line = get_line(request_socket)
    lines = []
    header_fields = dict()
    while True:
        line = get_line(request_socket)
        if line == '' or line == '\r\n':
            break
        lines.append(line)
        key, value = make_line_tuple(line)
        header_fields[key] = value
    return request_line, header_fields


# Returns one line from the socket (delimited of CRLF)
def get_line(request_sock):
    line = ''
    while not line.endswith('\r\n'):
        line += request_sock.recv(1)
    return line.strip()


# Returns the HTTP status code for a given request_line
#   request_line_split -- string containing the request line
#   Returns:
#       The HTTP status code (implemented possibilities: 200, 404, 501, 505) followed by CRLF
def make_status_code(request_line_split):
    status_code = 'HTTP/1.1 '
    if request_line_split[0] == 'GET':
        if request_line_split[2] == 'HTTP/1.1':
            if get_file_size(request_line_split[1]) is not None:
                status_code += '200 OK'
            else:
                status_code += '404 Not Found'
        else:
            status_code += '505 HTTP Version Not Supported'
    else:
        status_code += '501 Not Implemented'

    return status_code


# Converts a dictionary into the properly formatted HTTP equivalent.
#   dictionary -- the dictionary to be converted
#   Returns:
#       a properly formatted HTTP-header equivalent for inserting into HTTP response headers
def convert_dict(dictionary):
    http_dict = ''
    for key, value in dictionary.iteritems():
        http_dict += str(key) + ': ' + str(value) + '\r\n'
    return http_dict


# ** Do not modify code below this line.

# Utility functions
# You may use these functions to simplify your code.

# Try to guess the MIME type of a file (resource), given its path (primarily its file extension)
#	file_path -- string containing path to (resource) file, such as './abc.html'
#	Returns:
#		If successful in guessing the MIME type, a string representing the content type, such as 'text/html'
#		Otherwise, None
def get_mime_type(file_path):
    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


# Try to get the size of a file (resource) in bytes, given its path
#	file_path -- string containing path to (resource) file, such as './abc.html'
#	Returns:
#		If file_path designates a normal file, an integer value representing the the file size in bytes
#		Otherwise (no such file, or path is not a file), None
def get_file_size(file_path):
    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()

# DESCRIPTION:
# When handling an HTTP request the complete header of the request is retrieved from the request_socket.
# The request line is then parsed and the desired resource is altered so that it corresponds to the local path of that
# file (i.e. "/" --> "./index.html"). Using this new resource reference, an HTTP status code is determined based on the
# received request line. Next, a dictionary is created for the response header, and the resource data is read from the
# corresponding file. The dictionary for the response header is then converted to an HTTP-friendly string and is added
# to the string containing the response, with the read resource data appended to response string. Finally, the response
# string is sent to the requesting host, and the requesting socket is closed.
