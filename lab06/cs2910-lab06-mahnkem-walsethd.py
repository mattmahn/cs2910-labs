# coding=utf-8

# : ## Lab 6 ##
#:
#: CS-2910 Network Protocols
#: Dr. Sebern
#: Fall quarter 2014-2015
#:
#: | Team members            |
#: |:------------------------|
#: | Matt Mahnke   |
#: | Dylan Walseth |
#:
# If you are curious about the names above, look them up!

import imaplib
import base64
import quopri
import email
from email.parser import Parser

import mimetypes


# Open a file containing the full text of an email message (headers and all).
# Display the hierarchy of parts in the message.
# Save each "leaf" (non-multipart) part in a file.
def main():
    email_parser = Parser()
    with open('imaplib-mail.txt', 'rb') as input_file:
        email_message = email.message_from_file(input_file)
    for message_item in email_message.items():
        print '##Message item: {0} :: {1}'.format(message_item[0], message_item[1])
    print "Message part hierarchy:"
    # Process the message parts, starting with [1], which is rendered as "1"
    # The first child will be [1 1], rendered as "1.1"
    process_child_parts(email_message, [1])


# Recursive function to traverse (depth-first) the message part hierarchy.
def process_child_parts(part, part_num):
    part_type = part.get_content_type()

    guessed_extnsn = mimetypes.guess_extension(part_type)
    # Make the part number in dotted decimal notation
    part_num_str = str(part_num[0])
    for part_num_component in part_num[1:]:
        part_num_str += '.' + str(part_num_component)
    # print '{0} {1}'.format(part_num_str,part_type)

    # Display the header items for this part
    header_items = part.items()
    for item in part.items():
        print '{0} : {1} = {2}'.format(part_num_str, item[0], item[1])
    params = part.get_params()
    for param in params:
        param_value = '= ' + param[1] if param[1] else ''
        print '{0} : {1} {2}'.format(part_num_str, param[0], param_value)

    # If part is not multipart, it is a leaf part; process it.
    if not part.is_multipart():
        part_payload = part.get_payload()
        transfer_encoding = part.get('Content-Transfer-Encoding')

        # As it is, this code will dump each leaf part into a file named with its number.
        # To get the real data, it is necessary to decode the part_payload value
        # before writing it to the file.

        # This is where the decoding SHOULD happen
        part_payload = process_part(part_payload, transfer_encoding)

        if part_type == 'application/octet-stream':
            with open(param[1], 'wb') as attached_file:
                attached_file.write(part_payload)
        else:
            with open('data-' + part_num_str + guessed_extnsn, 'wb') as payload_file:
                payload_file.write(part_payload)

    #	print '{0} : Payload:\n--------\n{1}\n--------'.format(part_num_str,part_payload)

    # If part is multipart, recurse to children.
    else:
        children = []
        for child in part.walk():
            children.append(child)
        # print "children=",children
        child_num = 1
        for child2 in children[1:]:
            # print "child2=",child2.get_content_type()
            child_part_num = part_num[:]
            child_part_num.append(child_num)
            child_num += 1
            process_child_parts(child2, child_part_num)
    return


# Decode the part_payload value
#	part_payload -- the raw part text
# Return value
#	Decoded part, as a string
def process_part(part_payload, transfer_encoding):
    # Default is to do nothing
    decoded_payload = part_payload

    if transfer_encoding == 'base64':
        decoded_payload = base64.decodestring(part_payload)
    elif transfer_encoding == 'quoted-printable':
        decoded_payload = quopri.decodestring(part_payload)

    return decoded_payload


main()

# WHAT WAS LEARNED
# We noticed that many email headers don't follow a strict guideline; some can be omitted and applications can insert
# their own headers, without any problems.
#
# DESCRIPTION
# In the process_part() function, if transfer_encoding is "bas64" or "quoted-printable" the specified part_payload is
# decoded accordingly.
# When an attachment with a MIME type of application/octet-stream is found, mimetypes.guess_extension() usually cannot
# guess the correct extension, so the filename and extension of the attachment is used when writing the attachment
# to a file.
