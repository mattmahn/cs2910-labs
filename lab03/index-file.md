<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="/markdown/style.css"/>
    <meta name="content-type" http-equiv="content-type" value="text/html; utf-8"/>
	<script type="text/javascript"
	  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
	</script>
	<script type="text/x-mathjax-config">
	  MathJax.Hub.Config({
	    "HTML-CSS": {
	      availableFonts: ["TeX"]
	    }
	  });
	</script>
  </head>
  <body>
