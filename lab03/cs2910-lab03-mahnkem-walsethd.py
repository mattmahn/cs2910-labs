# coding=utf-8
# cs2910-lab03-mahnkem-walsethd.py Lab 3 -- HTTP client
# Team members: Matthew Mahnke (mahnkem), Dylan Walseth (walsethd)

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# import the "regular expressions" module
import re


def main():
    # this resource request should result in "chunked" data transfer
    get_http_resource('http://seprof.sebern.com/', 'index-file.md')
    # this resource request should result in "Content-Length" data transfer
    get_http_resource('http://seprof.sebern.com/sebern1.jpg', 'sebern1.jpg')


# another resource to try for a little larger and more complex entity
# get_http_resource('http://seprof.sebern.com/courses/cs2910-2014-2015/sched.md','sched-file.md')

# Get an HTTP resource from a server
# Parse the URL and call function to actually make the request.
#		url -- full URL of the resource to get
#		file_name -- name of file in which to store the retrieved resource
# (do not modify this function)
def get_http_resource(url, file_name):
    # Parse the URL into its component parts using a regular expression.
    url_match = re.search('http:(\d*)//([^/]*)(/.*)', url).groups()
    print 'url_match=', url_match
    if url_match and len(url_match) == 3:
        host_name = url_match[1]
        host_port = int(url_match[0]) if url_match[0] else 80
        host_resource = url_match[2]
        print 'host name = {0}, port = {1}, resource = {2})'.format(host_name, host_port, host_resource)
        make_http_request(host_name, host_port, host_resource, file_name)
    else:
        print 'get_http_resource: URL parse failed, request not sent'


# Get an HTTP resource from a server
#		host -- host name
#		port -- host port number
#		resource -- path/name of resource to get
#		file_name -- name of file in which to store the retrieved resource
def make_http_request(host, port, resource, file_name):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    request_line = 'GET {0} HTTP/1.1\r\n'.format(resource)
    request_line += 'Host: {0}\r\n\r\n'.format(host)
    # print request_line
    sock.send(request_line)
    header_dict = get_header(sock)

    bin_data = ''
    if ('Transfer-Encoding' in header_dict) and \
            (re.match('chunked.*', header_dict['Transfer-Encoding'])):
        bin_data = get_data(sock, None)
    elif 'Content-Length' in header_dict:
        bin_data = get_data(sock, int(header_dict['Content-Length'].strip(r'\rn')))

    sock.close()
    write_file(file_name, bin_data)


def get_header(sock):
    header_dict = dict()
    lines = []
    response = ''
    while True:
        response += sock.recv(1)
        if response == '\r\n':
            break
        elif response.endswith("\r\n"):
            field_resp = response.split(':')
            if len(field_resp) > 1:
                header_dict[field_resp[0].strip()] = str(field_resp[1:]).strip("'[\r\n \\]'")
            lines.append(response)
            response = ''

    return header_dict


# Returns raw data from the server
# 		sock - the socket from which to receive data
#		size - the number of bytes to receive from the socket, if size==None
#			   assume the data is chunked
def get_data(sock, size=None):
    data = ''
    if size is None:  # assume chunked data
        response = ''
        # the next line has the number of chunked bytes encoded in hexadecimal
        while True:
            response += sock.recv(1)
            # TODO this match *should* be right; verify it is so
            if re.match(r".*\r\n", response):
                break
        data = sock.recv(int(response, 16))
    else:  # this is when 'Conten-Length' was specified in the header fields
        data = sock.recv(size)

    return data


def write_file(file_name, data):
    file = open(file_name, 'wb')
    file.write(data)
    file.close()


main()

# When make_http_request(...) is called, a socket is created that connects to the
# specified server over TCP. The header of the GET responses are retrieved
# (using get_header(socket)) one byte at a time, until two CRLF's are found
# (we actually check for a single CRLF on its own line). The get_header(socket)
# method then returns a dictionary of the header fields. If 'Transfer-Encoding' is
# found in the header fields get_data(...) is called and receives the length of
# the chunked data, then recieves that amount of data from the socket. If
# 'Transfer-Encoding' is not found and 'Content-Length' is found in the header
# fields, get_data(...) recieves the number of bytes specified by 'Content-Length'
# from the socket. When the correct content data is retrieved from the HTTP
# server, the content is written to a file as raw binary.
#
# We were able to get the HTML/Markdown file, but Windows says the .jpeg is
# corrupted. When the .jpeg is viewed in a hex editor it appears to be similar
# to the same image download conventionally, through a browser.
