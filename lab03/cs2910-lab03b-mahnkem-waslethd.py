# coding=utf-8
# cs2910-lab03-mahnkem-walsethd.py Lab 3 -- HTTP client
# Team members: Matthew Mahnke (mahnkem), Dylan Walseth (walsethd)

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# import the "regular expressions" module
import re


def main():
    # this resource request should result in "chunked" data transfer
    get_http_resource('http://seprof.sebern.com/', 'index-file.md')
    # this resource request should result in "Content-Length" data transfer
    get_http_resource('http://seprof.sebern.com/sebern1.jpg', 'sebern1.jpg')


# another resource to try for a little larger and more complex entity
# get_http_resource('http://seprof.sebern.com/courses/cs2910-2014-2015/sched.md','sched-file.md')

# Get an HTTP resource from a server
# Parse the URL and call function to actually make the request.
# url -- full URL of the resource to get
#		file_name -- name of file in which to store the retrieved resource
# (do not modify this function)
def get_http_resource(url, file_name):
    # Parse the URL into its component parts using a regular expression.
    url_match = re.search('http:(\d*)//([^/]*)(/.*)', url).groups()
    print 'url_match=', url_match
    if url_match and len(url_match) == 3:
        host_name = url_match[1]
        host_port = int(url_match[0]) if url_match[0] else 80
        host_resource = url_match[2]
        print 'host name = {0}, port = {1}, resource = {2})'.format(host_name, host_port, host_resource)
        make_http_request(host_name, host_port, host_resource, file_name)
    else:
        print 'get_http_resource: URL parse failed, request not sent'


# Get an HTTP resource from a server
#		host -- host name
#		port -- host port number
#		resource -- path/name of resource to get
#		file_name -- name of file in which to store the retrieved resource
def make_http_request(host, port, resource, file_name):
    pass


# create socket and connect with the socket
# Send HTTP GET request
# Call get_header() to get a dictionary of the header fields
# If the entity is chunked, call get_chunks()
# Otherwise, call get_data()

# Close the socket
# Write the entity to a file using write_file()

# Converts the HTTP header fields to a dictionary and returns the dictionary
#     sock -- the socket from which to get the HTTP header
#
#     returns a dictionary representation of the header fields; if the recieved
#             HTTP status code is not 200, None is returned
def get_header(sock):
    # Read the header 1 byte at a time until the header is fully received
    status_line = get_line(sock)
    # If the HTTP status code is not 200, None is returned
    if not ('200' in status_line):
        return None
    lines = []
    # Convert the header fields to a dictionary
    while not lines[-1] == '':
        lines.append(get_line(sock))
    # Return dictionary representing the header fields;
    return dict(lines)


# Returns one line from the socket (delimited by CRLF)
def get_line(sock):
    line = ''
    while not line.endswith('\r\n'):
        line += sock.recv(1)
    return line.rstrip()  # rstrip() removes trailing whitespace


# Gets the amount of data specified by size from the server connected to sock.
#     sock -- the socket from which to request data
#     size -- the amount of data to receive (bytes)
#
#     returns the data received, or an empty string if size <= 0
def get_data(sock, size):
    data = ''
    requested_bytes_remain = size
    # Continues to receive the number of bytes remaining until we have received size bytes
    while requested_bytes_remain > 0:
        new_data = sock.recv(requested_bytes_remain)
        data += new_data
        requested_bytes_remain -= len(new_data)
    # Returns the received data
    return data


# Returns chunked data from a socket
#     sock -- the socket from which to request chunks
#
#     returns the chunked data
def get_chunks(sock):
    data = ''
    # Do the following until until we receive no more data (chunk size == 0)
    while True:
        # Read the hex-encoded chunk size & Convert the chunk size into decimal
        chunk_size = int(get_line(sock).strip(), 16)
        if chunk_size <= 0:
            break
        else:
            # Call get_data() passing in the decimal chunk size
            data += get_data(sock, chunk_size)
    # Returns all the chunked data
    return data


# Creates, or overwrites, a file in the working directory called file_name,
# writes binary data to the file.
def write_file(file_name, data):
    # Open file (creating/overwriting as necessary)
    file = open(file_name, 'wb')
    # Write data to file in binary mode
    file.write(data)
    # Close file
    file.close()


main()

# Please consider this for an adjustment of our previous grade
